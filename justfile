#!/usr/bin/env -S just --justfile

# List available commands.
_default:
    just --list --unsorted

# Set up sane defaults
setup:
    #!/bin/fish
    ln --verbose --symbolic ../capsules/astronaut-xdg-canonical-default.fish {{justfile_directory()}}/functions/
    ln --verbose --symbolic ../capsules/astronaut-xdg-gtk.fish {{justfile_directory()}}/functions/
    ln --verbose --symbolic ../capsules/astronaut-xdg-wget.fish {{justfile_directory()}}/functions/

    astronaut-xdg-canonical-default
    astronaut-xdg-gtk
    astronaut-xdg-wget
