
```
            _                               _
   __ _ ___| |_ _ __ ___  _ __   __ _ _   _| |_
  / _` / __| __| '__/ _ \| '_ \ / _` | | | | __|
 | (_| \__ \ |_| | | (_) | | | | (_| | |_| | |_
  \__,_|___/\__|_|  \___/|_| |_|\__,_|\__,_|\__|
  A powerful and lightweight shell environment
           for commandline explorers.
```

*By Joshua Langley*

Astronaut is a shell configuration for the powerful `fish` shell that provides features so powerful you can practically fly through the commandline. This can be achieved with plugin managers, but the objective here is to provide the same level of power with no abstraction, minimal setup and no hassle.

Long term, I also hope to provide some guides to help beginners get started with the UNIX commandline in a friendly and helpful way. I know the commandline can be confusing, especially if you're coming from Windows, but it doesn't have to be difficult and is often the most powerful and efficient way to get stuff done. My goal is for it to be helpful, not harmful to you.

NOTE: Astronaut is meant for the `fish` shell, which is only available for UNIX-like systems. If you're on Windows, you can't really get anything as good as a UNIX shell, but you have a few options that are better than nothing I guess. I'll work on detailing those options at some point.

## Installation

### Step 1

First, clone this repository to `$XDG_CONFIG_HOME/fish`. (This is normally `~/.config/fish`.) It will serve as your `fish` shell configuration folder.

```bash
git clone https://gitlab.com/joshlangley/astronaut.git ${XDG_CONFIG_HOME:-~/.config}/fish
```

### Step 2

Install the necessary programs and dependencies. You'll need the following things:

- [fish](https://fishshell.com/) - Friendly Interactive SHell.
- [starship](https://starship.rs/) - ☄🌌️ The minimal, blazing-fast, and infinitely customizable prompt for any shell!
- [zoxide](https://github.com/ajeetdsouza/zoxide) - A smarter cd command.
- [fzf](https://github.com/junegunn/fzf) - 🌸 A command-line fuzzy finder - Provides fuzzy finding in Zoxide.
- [Nerd Fonts](https://www.nerdfonts.com/) - Provides the nerdy icons used by nearly everything in this list.

Below are some commands for getting that stuff set up on popular platforms.

<details><summary>**Arch Linux** / Manjaro / EndeavorOS / other Arch-based</summary>

You lucky Arch user! For you, this part is as easy as a single command!


```bash
sudo pacman -Syu fish starship zoxide fzf pkgfile ttf-font-nerd
```
Choose your favorite nerd font when prompted with package options. You can see previews of the options at [https://www.nerdfonts.com/font-downloads](https://www.nerdfonts.com/font-downloads).

</details>

<details><summary>**Homebrew** (macOS users should use this)</summary>

If you're a macOS uesr and you don't already have Homebrew installed, you will need it. (You should probably learn to love it too!) You can install it with the following command, which I got from [Homebrew's homepage](https://brew.sh/):

```zsh
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Once you have Homebrew, you can install most of the dependencies with:


```zsh
brew install fish starship zoxide fzf
```

Finish up by choosing your favorite nerd font from [https://www.nerdfonts.com/font-downloads](https://www.nerdfonts.com/font-downloads) and download it. Install all of either the `.ttf` or the `.otf` font files however your desktop environment handles it. Normally this can be done by opening them from your file manager (like "Finder" on macOS for example) and choosing "Install" for the one (or many) dialog(s) that appear.

</details>

<details><summary>**Fedora**</summary>

Use the following command to install everything but the font. You may need enter your `sudo` password and keep track of various dialogs along the way. If something times out, just run it again and pay better attention this time... :)


```bash
sudo dnf install fish zoxide fzf cargo && curl -sS https://starship.rs/install.sh | sh
```

Then choose your favorite nerd font from [https://www.nerdfonts.com/font-downloads](https://www.nerdfonts.com/font-downloads) and download it. Install all of either the `.ttf` or the `.otf` font files. This can be done by multi-selecting and opening the files and choosing "Install" in the many dialogs which then appear.

</details>

<details><summary>**Ubuntu** / Ubuntu-based</summary>

Use the following command to install everything but the font. You may need enter your `sudo` password and keep track of various dialogs along the way. If something times out, just run it again and pay better attention this time... :)


```bash
sudo apt install fish zoxide fzf && curl -sS https://starship.rs/install.sh | sh
```

Then choose your favorite nerd font from [https://www.nerdfonts.com/font-downloads](https://www.nerdfonts.com/font-downloads) and download it. Install all of either the `.ttf` or the `.otf` font files. This can be done by multi-selecting and opening the files and choosing "Install" in the many dialogs which then appear.

</details>

### Step 3

Now you should change your shell to `fish`. You can do this with the `chsh` utility:

```bash
chsh --shell $(which fish)
```

Until you log out and back in, you'll need to run `fish` at your shell prompt. If you use a terminal emulator like Konsole, you may also have to change your shell in the profile settings.

## Capsules

Astronaut now uses little modular scripts that I called capsules. Capsules amount to fish functions in individual files. You can load certain function automatically when the shell loads by symlinking them from `/capsules/<name>.fish` to the `/functions/` directory. Then reload the shell functions using `source ~/.config/fish/config.fish`. (Replace with the path to your `config.fish` if needed.) Eventually you'll be able to automate this using the justfile.

Some simple, basic defaults can be loaded with `just setup`. See the output of `just` for more information.

Once you've loaded a capsule, you can run it like a command, or call it in `config.fish`.
