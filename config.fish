if status is-interactive
    # Commands to run in interactive sessions can go here

    # Starship shell prompt initialization.
    starship init fish | source
    # Set Starship config path
    set --export --universal STARSHIP_CONFIG "$XDG_CONFIG_HOME/fish/starship.toml"

    # Zoxide (smarter cd) initialization.
    zoxide init fish | source
    alias cd=z # Use Zoxide instead of fish's builtin cd.

end

