#!/bin/fish

# Sets Rust programming language utilities to be more XDG Base Directory compliant.
#   [https://wiki.archlinux.org/title/XDG_Base_Directory]
#   [https://www.rust-lang.org/]
#   [https://www.rust-lang.org/tools/install]
#   [https://wiki.archlinux.org/title/Rust]
#   [https://wikipedia.org/wiki/Rust]
function astronaut-xdg-rust
    set --export --universal CARGO_HOME "$XDG_DATA_HOME/cargo"
    set --export --universal RUSTUP_HOME "$XDG_DATA_HOME/rustup"
end
