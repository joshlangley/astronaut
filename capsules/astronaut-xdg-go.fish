#!/bin/fish

# Sets Golang utilities to be more XDG Base Directory compliant.
#   [https://wiki.archlinux.org/title/XDG_Base_Directory]
#   [https://go.dev/]
#   [https://wiki.archlinux.org/title/Go]
#   [https://wikipedia.org/wiki/Go_(programming_language)]
function astronaut-xdg-go
    set --export --universal GOPATH "$XDG_DATA_HOME/go"
    set --export --universal GOMODCACHE "$XDG_CACHE_HOME/go/mod"
end
