#!/bin/fish

# Load features for the antidot home cleanup utility
#   [https://github.com/doron-cohen/antidot]
function astronaut-load-antidot
    # Load author's shell completions
    antidot completion fish | source
end
