#!/bin/fish

# Load features for the just command runner
#   [https://just.systems]
function astronaut-load-just
    # Load author's shell completions
    just --completions fish | source
end
