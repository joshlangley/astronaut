#!/bin/fish

# Configures wget to be more XDG Base Directory compliant.
#   [https://wiki.archlinux.org/title/XDG_Base_Directory]
function astronaut-xdg-wget
    set --export --universal WGETRC "$XDG_CONFIG_HOME/wgetrc"
    alias wget 'wget --hsts-file "$XDG_CACHE_HOME/wget-hsts"'
end
