#!/bin/fish

# Sets the Unison data directory to be more XDG Base Directory compliant.
#   [https://wiki.archlinux.org/title/XDG_Base_Directory]
#   [https://github.com/bcpierce00/unison]
function astronaut-xdg-unison
    set --export --universal UNISON "$XDG_DATA_HOME/unison"
end
