#!/bin/fish

# XDG Canonical Defaults
#
# In 2003, the XDG specification was released to address the hard coded nature
#   of various program data on Linux, especially configuration and data folder
#   paths. At the time, such paths were often hard coded to something in the
#   root of the current user's home directory. Decades later, many programs now
#   support this specification, though support is often hit or miss. See
#   [XDG Base Directory](https://wiki.archlinux.org/title/XDG_Base_Directory)
#   for more details.
#
#   Though many programs do not use the specification by default (therefore not
#   fully XDG compliant), they often do honor the XDG base directory variables
#   if they are set. This function will explicitly set these variables to their
#   defaults to circumvent this undesirable behavior.
#
#   It's worth pointing out that the XDG base directory specification defines
#   the values programs should default to if not explicitly set, but on
#   operating systems other than Linux these values usually do not match the
#   system's canonical paths, and can even cause issues. Therefore this function
#   will detect the appropriate values for the current system and use the
#   appropriate canonical defaults *instead* of the specification defaults.

# **For a very clean home directory:** Many programs do not honor the XDG base
#   directory specification at all, but *do* allow users to specify a config
#   and/or data path one way or another. Astronaut provides many capsules to
#   force these programs to behave as if they were XDG compliant, allowing you
#   to clean up your home directory even more. Most of these capsules are based
#   on the compatibility tables on the XDG Base Directory ArchWiki page. These
#   capsules follow the `astronaut-xdg-<program>.fish` naming pattern and can be
#   enabled on a per-program basis as needed. Some of the ones that affect
#   everyone are enabled by default, but they are not all enabled by default in
#   order to improve shell initialization times.

# TODO: Investigate the possibility of using the `xdg-user-directories` utility,
#   at least on Linux, but where ever it's available.
#   See [https://wiki.archlinux.org/title/XDG_user_directories] for more info.

# Sets the user XDG base directory environment variables to the canonical defaults.
#   [https://wiki.archlinux.org/title/XDG_Base_Directory]
#   [https://gist.github.com/roalcantara/107ba66dfa3b9d023ac9329e639bc58c]
function astronaut-xdg-canonical-default
    switch (uname)
        case Linux
            # XDG Specification defaults (Linux canonical)
            set --export --universal XDG_CONFIG_HOME "$HOME/.config"
            set --export --universal XDG_CACHE_HOME "$HOME/.cache"
            set --export --universal XDG_DATA_HOME "$HOME/.local/share"
            set --export --universal XDG_STATE_HOME "$HOME/.local/state"
        case Darwin
            # macOS canonical defaults
            set --export --universal XDG_CONFIG_HOME "$HOME/Library/Preferences"
            set --export --universal XDG_CACHE_HOME "$HOME/Library/Caches"
            set --export --universal XDG_DATA_HOME "$HOME/Library/Application Support"
            set --export --universal XDG_STATE_HOME "$HOME/Library/Application Support"
        case '*'
            # System not known, give the user a heads up that the expected
            #   XDG compliant behavior won't be present as a result.
            echo "Astronaut: xdg-canonical-defaults: Could not detect system type, so the canonical XDG Base Directory paths are not being set! Expect XDG non-compliant behavior from some programs!"
        # case Redmond
            # Nvm. Fish is too good for this one. After all, if you wanted a
            # good shell, would you even be using this system?
            #   Jk, Jk...                                  ...not really though.
    end
end
