#!/bin/fish

# Sets old Gtk versions to be more XDG Base Directory compliant.
#   [https://wiki.archlinux.org/title/XDG_Base_Directory]
function astronaut-xdg-gtk
    set --export --universal GTK_RC_FILES "$XDG_CONFIG_HOME/gtk-1.0/gtkrc"
    set --export --universal GTK2_RC_FILES "$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
end
