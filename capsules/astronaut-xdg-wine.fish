#!/bin/fish

# Puts the default wineprefix in a XDG Base Directory compliant spot.
#   [https://wiki.archlinux.org/title/XDG_Base_Directory]
function astronaut-xdg-wine
    set --export --universal WINEPREFIX "$XDG_DATA_HOME/wineprefixes/default"
end
