#!/bin/fish

# Load Atuin shell history manager
#   (https://atuin.sh/)
function astronaut-load-atuin
    # Load Atuin.
    atuin init fish --disable-up-arrow | source
end
